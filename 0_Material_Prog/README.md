Projeto de Aulas


## Aula 1	Lógica de Programação com PHP I
- O que é PHP?
- Instalando as aplicações  no Windows;
- Instalando as aplicações  no Linux;
- Explicando os tipos de dados: Inteiro, ponto flutuante, string, booleano;
- Operações de atribuição;
- Operações aritméticas, incremento e decremento;
- Operações lógicas e de comparação;

## Aula 2	Lógica de Programação com PHP II
- Estrutura de condição IF e SWITCH;
- Estrutura de repetição FOR, WHILE e DO...WHILE;
- Funções básicas para hora;
- Funções básicas matemáticas;

## Aula 3	Tratamento de formulários HTML com PHP
- Rápida introdução sobre formulários HTML;
- Explicando PHP sendo executado no servidor e HTML retornado sendo manipulado no cliente;
- Criação de formulários;

## Aula 4	Formulários
- Recebendo dados enviados pelo formulário;
- Explicando diferenças entre os métodos GET e POST;
- Cookies e Sessions;
- A função MAIL;

## Aula 5	Leitura e escrita de dados em arquivos I
- A função fopen e modos de abertura [e permissões];
- As funções fwrite e fputs;
- As funções fread, fgets, fgetc e file();
- A função fclose;
- Utilizando formulários e arquivos

## Aula 6	Criação e uso de funções em PHP
- Delimitando funções
- Modularização, utilizando múltiplos arquivos
- Cinto de utilidades, conhecendo as funções built-in do PHP

## Aula 7	Leitura e escrita de dados em arquivos II
- Criação de um banco de dados csv
- Aspectos avançados de manipulação de arquivos (criação de diferentes tipos de arquivo)

## Aula 8	Teste prático
- Exercício para testar todo o conteúdo dado nas sete primeiras aulas;


Parte deste conteúdo programático foi criado com base no curso da UFPR (http://www.inf.ufpr.br/instrutores/c_php.html).